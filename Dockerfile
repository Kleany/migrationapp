FROM python

WORKDIR /flask_app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY ./flask_app .
USER root

CMD ["flask", "run"]