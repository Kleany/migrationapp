# Git Migration App
## Overview

**Git Migration App** - is a tool for an automated cloning of any given repository
from any remote to your [Gitea](https://about.gitea.com) server. The tool is
implemented as a web-application based on a Flask framework.

It allows you to migrate any given repository of any given commit SHA-1 with
its whole hierarchy of external dependencies (if there are any).

## Issue

Let's assume your company develops some software product and stores all of its
source code in a remote repositories (GitHub, GitLab, etc.).
Imagine someday you wanted to deploy your own git server to store all source
code your company uses.

At first glance, it doesn't seem to be difficult. Just deploy the server,
clone your repositories, set new remote URLs and push them to the new remotes.
However, your projects may use the functionality of external libraries
(see [Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)).
Then it appears you need to migrate them as well. But they can also use
other libraries you didn't even know about! In that way, such a migration
can be very tricky to realize. For a visual representation of a problem see the
picture below:

![Problem](./readme/en/readme_1.drawio.svg)

It would be acceptable, if it only required to migrate all the repositories
one by one. But there is another problem. It appears that submodules in git
are specified by two components - URL address of a submodule repository
and its commit SHA-1.

![Git submodules](./readme/en/readme_2.drawio.svg)

If you try to manually migrate all the submodules without any other actions,
your target repository will appear in non-working condition. Because its
submodules' configurations remained the same, they will point to old,
non-actual URL-addresses. See the following picture to visualize a problem:

![URL problem](./readme/en/readme_3.drawio.svg)

If a repository has submodules, its URL are specified in a special
`.gitmodules` file in a root of a repository. It has the following syntax
(the example is taken from [this repository](https://github.com/tianocore/edk2.git)):
```.gitmodules
...
[submodule "CryptoPkg/Library/OpensslLib/openssl"]
	path = CryptoPkg/Library/OpensslLib/openssl
	url = https://github.com/openssl/openssl
[submodule "BaseTools/Source/C/BrotliCompress/brotli"]
	path = BaseTools/Source/C/BrotliCompress/brotli
	url = https://github.com/google/brotli
	ignore = untracked
...
```

So, it would seem that you just need to change these URL addresses to new ones.
But as you may guess, editing contents of `.gitmodules` file will require to
make a new commit to store changes. In that way, the commit you are to push
to a new remote will be different. Moreover, you will need to make such a commit
in every repository that has submodules in it. If migration is operated
this way, you will get the following:

![SHA-1 problem](./readme/en/readme_4.drawio.svg)

Now your upper level repositories point to incorrect commits, where URLs
hasn't been changed yet. Make note that the repositories from the lowest level
of the submodules' hierarchy are migrated properly, as they haven't needed
a new commit. So, in addition to the URLs, you also need to change commits
SHA-1 referenced by the upper level repositories. This fact also requires
that you start migration process from the lowest level of the target repository's
submodules' hierarchy (because you need to change commits 'on a go').

It also appears, that changing these commit SHA-1 references isn't that easy
as regular file editing. You could have changed these refs manually with
git commands (see [git submodule update](https://git-scm.com/docs/git-submodule)),
if only the whole hierarchy was cloned recursively into one big repository.
But surely the size of that kind of repository can be more than overwhelming.
So, this way is inappropriate.

There is a way to do this without recursive cloning, but it inflicts using
[low-level git commands](https://git-scm.com/docs/git#_low_level_commands_plumbing),
which is very inconvenient to do manually.

Thus, this application automates the whole migration process for your convenience!

## Installation

To run the application you will need [Docker](https://www.docker.com/) software installed.
The installation process consists of two stages: *[Server preparation](#1-server-preparation)*
and *[Application deployment](#2-application-deployment)*.

### 1. Server preparation

This project implies Gitea server deployment in a **Docker** container,
but you can use any other method (see [Gitea installation](https://docs.gitea.com/category/installation)).
If you have decided to use Docker, you may use [this](docker-compose.yml) **Docker
Compose** configuration to run everything you need.

Before launching server for the first time go to [configuration file](.env)
and specify variables like the following:
```
# The host your Gitea server is located (IP or DNS address)
    SERVER__HOST=<ip>

# The port of your Gitea server (leave empty if absent)
    SERVER__PORT=<port>
```

Other variables can be left unspecified.

After `.env` file is configured, you can start the container with respective
Docker Compose profile:
```bash
docker compose --profile server up
```

Finalize server installation and configuration according to
[official guide](https://docs.gitea.com/installation/install-with-docker#installation).

> If you already have Gitea server installed just specify
> server variables according to your server configuration.

### 2. Application deployment

Regardless of the server deployment method you have chosen
further steps are all the same.

Once the server is launched, login as an admin user (or register one).
Then go to Settings > Applications > Manage Access Tokens. There you have to
generate an access token for your admin user:
1. Enter token's name;
2. Set its "Repository and Organization Access" to "All";
3. In "Select permissions" drop-down list set all permissions to
"Read and Write" state;
4. Press the "Generate Token" button.
![Token generation](./readme/en/readme_1.png)

If successful you will get the following:
![Token](./readme/en/readme_2.png)

Copy the generated token to your `.env` file here:
```
# The access token of your admin user on the server (fill once you have generated one)
    SERVER__TOKEN=<token>
```

You should also specify these variables responsible for your admin user's name
and email:
```
# Your admin user's username
    SERVER__ADMIN_USERNAME=<username>

# Your admin user's email
    SERVER__ADMIN_EMAIL=<email>
```

The only thing left is to specify Flask application variables:
```
# The host Git Migration App should be run
    FLASK_RUN_HOST=<ip>

# The port for application
    FLASK_RUN_PORT=<port>

# Application environment (can be set to 'development' or 'production')
    FLASK_ENV=<preset>

# Flask's secret key ( keep it in secret :) )
    FLASK_SECRET_KEY=<secret>
```

Then run the app container with the following command:
```bash
docker compose --profile app up
```

The installation is complete. 
Now you can visit app's URL `http://FLASK_RUN_HOST:FLASK_RUN_PORT`.

## Usage

Main functionality of the application is accessible by
the link "Migrate a repo". There's a form to enter URL address and
commit SHA-1 of a repository to be migrated.

Go to the web-page of the target repository. There you have to copy its
clone web URL (using https protocol) and any desired commit SHA-1.

Let's take [this repository](https://github.com/tianocore/edk2.git)
for example:

![Repo URL](./readme/en/readme_3.png)
![Repo SHA-1](./readme/en/readme_4.png)

Copy the parameters into the forms and press the "Enter" button.
After the scanning process is finished you will be redirected to the page
with graphical representation of repository's hierarchy. Press the
"Migrate" button to initialize migration sequence.

If migration is successful, you can visit your server to find the target
repository available in fully working condition.

Logs can be found in the `migrationapp/flask_app/app.log` file.

> On the main page you can also find the "Gitea server API" link. It
> provides manual access to application's server API handlers. You can
> use them if you want to automatically manipulate the server. This
> feature is likely to be hidden in the future.

## License

Distributed under the MIT License. See [LICENSE.txt](./LICENSE.txt) for more information.

## Contact

#### Makarenko Nikita

* Telegram: https://t.me/Kleanie
* Email: maknik1305@gmail.com

Project link: https://gitlab.com/Kleany/migrationapp.git

