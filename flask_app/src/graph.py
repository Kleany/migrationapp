from treelib import Tree
import logging


def make_graph(graph: list, target: str) -> str:
    logging.info(f'Making repository [{target}] graph hierarchy..')
    tree = Tree()
    logging.info(f'Adding root node [{target}]')
    logging.warning(f'{graph}')
    tree.create_node(target, target)
    for layer in graph:
        for node in layer:
            logging.info(f'Adding node [{node}] to parent [{layer[node]}]')
            tree.create_node(node, node, parent=layer[node])
    hierarchy = tree.show(stdout=False)
    return hierarchy
