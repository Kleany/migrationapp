import requests
import json
import logging


class GiteaManager:
    def __init__(self, config: dict):
        self.method = config['SERVER__METHOD']
        self.host = config['SERVER__HOST']
        self.port = config['SERVER__PORT']
        self.base_url = config['SERVER__BASE_URL']
        self.token = config['SERVER__TOKEN']

    def __curl__(self, method: str, body: dict, route: str, **kwargs):
        parameters = kwargs.get('parameters')
        url = self.method + '://' + self.host + ':' + self.port + self.base_url + route + '?token=' + self.token
        if parameters is not None:
            for name in parameters:
                url = url + '&' + name + '=' + parameters[name]
        header = {'Content-Type': 'application/json', 'accept': 'application/json'}
        response = requests.request(method=method, url=url, headers=header, json=body)
        if response.ok is False:
            raise ValueError(response.status_code, response.text)
        if response.text:
            return json.loads(response.text)
        else:
            return dict({})

    def clean_server(self):
        dest1 = '/orgs'
        orgs = self.__curl__(method='GET', body={}, route=dest1)
        for org in orgs:
            dest2 = '/orgs/' + org['name'] + '/repos'
            repos = self.__curl__(method='GET', body={}, route=dest2)
            for repo in repos:
                self.delete_repo(org['name'], repo['name'])
            self.delete_org(org['name'])

    def get_branch(self, username: str, reponame: str, branch: str):
        dest = '/repos/' + username + '/' + reponame + '/branches/' + branch
        try:
            logging.info(f'Getting branch [{branch}] in repo [{reponame}] in org [{username}]')
            response = self.__curl__(method='GET', body={}, route=dest)
        except ValueError as err:
            logging.warning(f'Failed to get branch [{branch}] in repo [{reponame}] in org [{username}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Branch [{branch}] in repo [{reponame}] in org [{username}] got successfully')
            return response

    def get_commit_list(self, username: str, reponame: str, branch: str):
        dest = '/repos/' + username + '/' + reponame + '/commits'
        parameters = {'sha': branch, 'stat': 'false', 'verification': 'false', 'files': 'false'}
        try:
            logging.info(f'Getting commit list from branch [{branch}] in repo [{reponame}] in org [{username}]')
            response = self.__curl__(method='GET', body={}, route=dest, parameters=parameters)
        except ValueError as err:
            logging.warning(f'Failed to get commit list from branch [{branch}] in repo [{reponame}] in org [{username}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Commit list from branch [{branch}] in repo [{reponame}] in org [{username}] got successfully')
            return response

    def create_org(self, name: str):
        data = {'username': name}
        try:
            logging.info(f'Creating org [{name}]')
            response = self.__curl__(method='POST', body=data, route='/orgs')
        except ValueError as err:
            logging.warning(f'Failed to create org [{name}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Org [{name}] created successfully')
            return response

    def delete_org(self, name: str):
        dest = '/orgs/' + name
        try:
            logging.info(f'Deleting org [{name}]')
            response = self.__curl__(method='DELETE', body={}, route=dest)
        except ValueError as err:
            logging.warning(f'Failed to delete org [{name}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Org [{name}] deleted successfully')
            return response

    def get_org(self, name: str):
        dest = '/orgs/' + name
        try:
            logging.info(f'Getting org [{name}]')
            response = self.__curl__(method='GET', body={}, route=dest)
        except ValueError as err:
            logging.warning(f'Failed to get org [{name}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Org [{name}] got successfully')
            return response

    def create_repo(self, username: str, reponame: str):
        data = {'name': reponame}
        dest = '/orgs/' + username + '/repos'
        try:
            logging.info(f'Creating repo [{reponame}] in org [{username}]')
            response = self.__curl__(method='POST', body=data, route=dest)
        except ValueError as err:
            logging.warning(f'Failed to create repo [{reponame}] in org [{username}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Repo [{reponame}] in org [{username}] created successfully')
            return response

    def get_repo(self, username: str, reponame: str):
        dest = '/repos/' + username + '/' + reponame
        try:
            logging.info(f'Getting repo [{reponame}] in org [{username}]')
            response = self.__curl__(method='GET', body={}, route=dest)
        except ValueError as err:
            logging.warning(f'Failed to get repo [{reponame}] in org [{username}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Repo [{reponame}] in org [{username}] got successfully')
            return response

    def delete_repo(self, username: str, reponame: str):
        dest = '/repos/' + username + '/' + reponame
        try:
            logging.info(f'Deleting repo [{reponame}] in org [{username}]')
            response = self.__curl__(method='DELETE', body={}, route=dest)
        except ValueError as err:
            logging.warning(f'Failed to delete repo [{reponame}] in org [{username}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Repo [{reponame}] in org [{username}] deleted successfully')
            return response

    def get_commit(self, username: str, reponame: str, commit: str):
        dest = '/repos/' + username + '/' + reponame + '/git/commits/' + commit
        try:
            logging.info(f'Getting commit [{commit}] in repo [{reponame}] in org [{username}]')
            response = self.__curl__(method='GET', body={}, route=dest)
        except ValueError as err:
            logging.warning(f'Failed to get commit [{commit}] in repo [{reponame}] in org [{username}]')
            logging.warning(f'Status code: [{err.args[0]}]')
            raise
        else:
            logging.info(f'Commit [{commit}] in repo [{reponame}] in org [{username}] got successfully')
            return response
