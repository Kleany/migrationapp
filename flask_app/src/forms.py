from wtforms import Form, StringField, validators


class OrgForm(Form):
    username = StringField('OrgName', [validators.Length(min=1, max=40),
                                       validators.DataRequired(),
                                       validators.Regexp(r'[a-zA-Z0-9][a-zA-Z0-9_.-]*')])


class RepoForm(Form):
    username = StringField('OrgName', [validators.Length(min=1, max=40),
                                       validators.DataRequired(),
                                       validators.Regexp(r'[a-zA-Z0-9][a-zA-Z0-9_.-]*')])

    reponame = StringField('RepoName', [validators.Length(min=1, max=100),
                                        validators.DataRequired(),
                                        validators.Regexp(r'[a-zA-Z0-9_.-]+')])


class CommitForm(Form):
    username = StringField('OrgName', [validators.Length(min=1, max=40),
                                       validators.DataRequired(),
                                       validators.Regexp(r'[a-zA-Z0-9][a-zA-Z0-9_.-]*')])

    reponame = StringField('RepoName', [validators.Length(min=1, max=100),
                                        validators.DataRequired(),
                                        validators.Regexp(r'[a-zA-Z0-9_.-]+')])

    commit = StringField('CommitSHA', [validators.Length(min=5, max=40),
                                       validators.DataRequired(),
                                       validators.Regexp(r'[0-9a-f]*')])


class URLForm(Form):
    url = StringField('RepoURL', [validators.DataRequired(),
                                  validators.Regexp(
                                      R'http(s)?://(([\w\-\.]+\.[\w]+)|(([\d]{1,3}.){3}[\d]{1,3}:[\d]{1,5}))/[\d\w]['
                                      R'\d\w_.-]*/[\d\w_.-]+(\.git(/)?)?')])

    sha = StringField('CommitSHA', [validators.Length(min=5, max=40),
                                       validators.DataRequired(),
                                       validators.Regexp(r'[0-9a-f]*')])
