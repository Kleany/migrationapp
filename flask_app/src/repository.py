from subprocess import PIPE, run, Popen
import logging
import shutil
import os
import re
from time import sleep


class Repository:
    def __init__(self, config: dict, url: str, required_sha1: str, level: int, gitea):
        self.user: str = url.split('/')[-2]
        self.name: str = url.split('/')[-1].split('.')[0]

        self.old_url: str = url

        url_domain: str = str(config['SERVER__HOST'])
        if str(config['SERVER__PORT']) is not None:
            url_domain += ':' + str(config['SERVER__PORT'])
        url_domain += '/' + self.user + '/' + self.name + '.git'
        self.new_url: str = str(config['SERVER__METHOD']) + '://' + url_domain
        self.remote_url: str = str(config['SERVER__METHOD']) + '://' + str(config['SERVER__TOKEN']) + '@' + url_domain

        self.old_sha: str = required_sha1
        self.old_sha_short: str = self.old_sha[0:6]

        self.level: int = level
        self.dir: str = str(config['APP__DIR']) + '/tmp/' + self.name + '_' + self.old_sha_short

        self.old_tree = None
        self.new_tree = None
        self.new_sha = None

        self.children = dict()  # {name : {name, url, path, old_sha1, new_sha1}}
        self.gitea = gitea

        self.admin_name = str(config['SERVER__ADMIN_USERNAME'])
        self.admin_mail = str(config['SERVER__ADMIN_EMAIL'])

        self.has_children: bool = False
        self.is_duplicate: bool = False

        logging.info(f'=====Repository object created with level [{self.level}]')

    def clone(self):
        # Preparing a directory for the repo
        if os.path.isdir(self.dir):
            # If repo has a duplicate in current hierarchy
            self.dir = self.dir + '_dup'
            self.is_duplicate = True
        os.makedirs(self.dir)

        # Fetching repo's specified commit with truncated history and with only the files in the root of the repo
        logging.info(f'Start fetching repo [{self.name}] on url [{self.old_url}] to dir [{self.dir}]..')
        run(['git', 'init'], capture_output=True, cwd=self.dir)
        run(['git', 'sparse-checkout', 'init', '--cone'], capture_output=True, cwd=self.dir)
        run(['git', 'remote', 'add', 'origin', self.old_url], capture_output=True, cwd=self.dir)
        output = run(['git', 'fetch', '--depth=1', 'origin', self.old_sha],
                     text=True, capture_output=True, cwd=self.dir)

        # Interrupting if fetching wasn't successful
        if output.returncode != 0:
            logging.error(f'Failed to fetch repo [{self.name}]')
            raise ValueError(output.returncode, output.stderr.strip())
        else:
            logging.info(f'Repo [{self.name}] commit [{self.old_sha}] fetch successful')

        # Checkout the required commit
        logging.info(f'Trying to checkout commit [{self.old_sha_short}] in repo [{self.name}]..')
        output = run(['git', 'checkout', self.old_sha], capture_output=True, text=True, cwd=self.dir)

        # Interrupting if checkout wasn't successful
        if output.returncode != 0:
            logging.error(f'Failed to checkout commit [{self.old_sha_short}]\n{output.stderr.strip()}')
            raise ValueError(output.returncode, output.stderr.strip())
        else:
            logging.info(f'Commit [{self.old_sha_short}] checkout successful')

        # Getting git-tree sha1 of a current commit
        logging.info(f'Getting git-tree sha of commit [{self.old_sha_short}]..')
        cat = Popen(['git', 'cat-file', 'commit', self.old_sha], stdout=PIPE, cwd=self.dir)
        grep = Popen(['grep', '-E', 'tree [a-z0-9]{40}'], stdin=cat.stdout, stdout=PIPE, text=True)
        cat.stdout.close()
        output = grep.communicate()[0].strip()
        self.old_tree: str = output.split('\n')[0].split(' ')[-1]
        logging.info(f'Successful getting git-tree sha [{self.old_tree}]')

    def get_children_info(self):
        # Looking for .gitmodules file in a repo
        logging.info(f'Looking for .gitmodules file in repo [{self.name}]..')

        ls = Popen(['ls', '-a'], stdout=PIPE, cwd=self.dir)
        grep = Popen(['grep', '.gitmodules'], stdin=ls.stdout, stdout=PIPE, text=True)
        ls.stdout.close()
        output = grep.communicate()[0].strip()
        content = output.split('\n')

        # Getting submodules info if presented
        if '.gitmodules' in content:
            # Setting submodules flag
            logging.info(f'File .gitmodules found in repo [{self.name}]')
            self.has_children = True

            # Getting submodule-object lines from current tree
            ls = Popen(['git', 'ls-tree', '-r', self.old_tree], stdout=PIPE, cwd=self.dir)
            grep = Popen(['grep', '160000 commit'], stdin=ls.stdout, stdout=PIPE)
            sed = Popen(['sed', 's/160000 commit //'], stdin=grep.stdout, stdout=PIPE, text=True)
            ls.stdout.close()
            grep.stdout.close()
            output = sed.communicate()[0].strip()
            submodule_lines = output.split('\n')

            # Getting submodule urls from .gitmodules file
            cat = Popen(['cat', '.gitmodules'], stdout=PIPE, cwd=self.dir)
            grep = Popen(['grep', 'url'], stdin=cat.stdout, stdout=PIPE)
            sed1 = Popen(['sed', 's/\t//'], stdin=grep.stdout, stdout=PIPE)
            sed2 = Popen(['sed', 's/url = //'], stdin=sed1.stdout, stdout=PIPE, text=True)
            cat.stdout.close()
            grep.stdout.close()
            sed1.stdout.close()
            output = sed2.communicate()[0].strip()
            submodule_urls = output.split('\n')

            cat = Popen(['cat', '.gitmodules'], stdout=PIPE, cwd=self.dir)
            grep = Popen(['grep', 'path'], stdin=cat.stdout, stdout=PIPE)
            sed1 = Popen(['sed', 's/\t//'], stdin=grep.stdout, stdout=PIPE)
            sed2 = Popen(['sed', 's/path = //'], stdin=sed1.stdout, stdout=PIPE, text=True)
            cat.stdout.close()
            grep.stdout.close()
            sed1.stdout.close()
            output = sed2.communicate()[0].strip()
            submodule_paths = output.split('\n')

            # Gathering submodules info
            for i in range(len(submodule_paths)):
                path = submodule_paths[i]
                url = submodule_urls[i]
                name = path.split('/')[-1]
                self.children[path] = {'url': url, 'path': path, 'name': name}

            for line in submodule_lines:
                subline = line.split('\t')
                self.children[subline[1]]['old_sha'] = subline[0]

            logging.info(f'Info on [{self.name}]\'s children:')
            for child in self.children:
                logging.info(f'{child} : {self.children[child]}')
        else:
            logging.info(f'File .gitmodules not found in repo [{self.name}]')

        return self.children

    def rm_repo(self):
        logging.info(f'Deleting repo [{self.dir}] directory..')
        shutil.rmtree(self.dir)

    def prepare_server(self):
        # Checking if organization already exists
        try:
            self.gitea.get_org(self.user)
        except ValueError as err:
            if err.args[0] == 404:
                self.gitea.create_org(self.user)
            else:
                raise

        # Checking if repo already exists
        try:
            self.gitea.get_repo(self.user, self.name)
        except ValueError as err:
            if err.args[0] == 404:
                self.gitea.create_repo(self.user, self.name)
            else:
                raise

    def migrate(self, prev_level: list):
        # Checking if migration is necessary
        try:
            logging.info(f'Checking if migration is necessary')
            if self.is_duplicate:
                sleep(5)
            self.gitea.get_branch(self.user, self.name, 'main-' + self.old_sha_short)
        except ValueError as err:
            if err.args[0] == 404:
                # Repo is not presented on new remote - preparing Gitea remote for further migration
                logging.info(f'Migration IS necessary')
                self.prepare_server()
            else:
                raise
        else:
            # Repo is already presented on new remote - getting sha of migrated commit
            logging.info(f'Migration IS NOT necessary')
            commits = self.gitea.get_commit_list(self.user, self.name, 'main-' + self.old_sha_short)
            for commit in commits:
                if commit['commit']['author']['name'] == 'tech_git':
                    if commit['commit']['message'] == 'Migration commit\n':
                        self.new_sha = commit['sha']
                        logging.info(f'New SHA for repo got from new remote [{self.new_sha}]')
                        logging.info(f'Migration passed')
                        return

        # Changing remote url to local server
        run(['git', 'remote', 'set-url', 'origin', self.remote_url], capture_output=True, cwd=self.dir)
        logging.info(f'New remote URL of repo [{self.name}] set to [{self.new_url}]')

        # Changing submodule objects commit references
        if self.has_children:
            # Updating self.children dictionary with new_sha's
            for name in self.children:
                for repo in prev_level:
                    if self.children[name]['old_sha'] == repo.old_sha and self.children[name]['url'] == repo.old_url:
                        self.children[name]['new_sha'] = repo.new_sha

            # Editing submodule-objects commit references
            logging.info(f'Starting to change commit references..')
            cur_tree = self.old_tree
            for name in self.children:
                trees, content = self._ls_tree(name, cur_tree)
                cur_tree = self._mk_tree(name, trees, content)
            self.new_tree = cur_tree
            fixed_sha = self._hash_commit()

            # Replacing existing commit with a new fixed one
            logging.info(f'Replacing commit [{self.old_sha}] with [{fixed_sha}]')
            run(['git', 'replace', self.old_sha, fixed_sha], capture_output=True, cwd=self.dir)
            run(['git', 'reset', '--hard'], capture_output=True, cwd=self.dir)
        else:
            logging.info(f'No submodules presented, changing commit references not needed')

        # Checkout new branch for further migration
        run(['git', 'checkout', '--orphan', 'migration'], capture_output=True, cwd=self.dir)

        # Changing urls in .gitmodules file
        if self.has_children:
            logging.info(f'Starting to edit urls in .gitmodules file')
            with open(self.dir + '/.gitmodules', 'r+') as f:
                file = f.read()
                for name in self.children:
                    for repo in prev_level:
                        if self.children[name]['url'] == repo.old_url and self.children[name]['old_sha'] == repo.old_sha:
                            file = re.sub(repo.old_url, repo.new_url, file)
                f.seek(0)
                f.write(file)
                f.truncate()
        else:
            logging.info(f'No submodules presented, editing urls in .gitmodules file not needed')

        # Commiting changes
        logging.info(f'Commiting changes..')
        run(['git', 'config', 'user.email', self.admin_mail], capture_output=True, cwd=self.dir)
        run(['git', 'config', 'user.name', self.admin_name], capture_output=True, cwd=self.dir)
        output = run(['git', 'commit', '-am', 'Migration commit'], capture_output=True, text=True, cwd=self.dir)

        # Checking if commit was successful
        if output.returncode != 0:
            logging.error(f'Failed to commit changes')
            raise ValueError(output.returncode, output.stderr)
        else:
            logging.info(f'Commit successful')

        # Saving new commit sha for parents
        log = Popen(['git', 'log', '--no-decorate'], stdout=PIPE, cwd=self.dir)
        grep = Popen(['grep', '-E', 'commit [a-z0-9]{40}'], stdin=log.stdout, stdout=PIPE, text=True)
        log.stdout.close()
        output = grep.communicate()[0].strip()
        self.new_sha = output.split(' ')[1]
        logging.info(f'New commit sha for the repo [{self.new_sha}]')

        # Pushing repo to new remote
        output = run(['git', 'push', '--set-upstream', 'origin', 'migration:main-' + self.old_sha_short],
                     capture_output=True, cwd=self.dir)

        # Checking if push was successful
        if output.returncode != 0:
            logging.error(f'Failed to push repo [{self.name}] to new remote\n{output.stderr[:-1]}')
            raise ValueError(output.returncode, output.stderr)
        else:
            logging.info(f'Repo [{self.name}] pushed to [{self.new_url}] successfully')

    def _get_tree(self, sha: str):
        cat = Popen(['git', 'cat-file', 'commit', sha], stdout=PIPE, cwd=self.dir)
        grep = Popen(['grep', '-E', 'tree [a-z0-9]{40}'], stdin=cat.stdout, stdout=PIPE)
        cat.stdout.close()
        tree = grep.communicate()[0].decode().strip().split(' ')[-1]
        return tree

    def _ls_tree(self, name: str, tree: str):
        path = self.children[name]['path']
        dirs = path.split('/')

        tree_objs = len(dirs)
        cur_tree = tree
        trees = []

        if tree_objs > 1:
            for i in range(tree_objs - 1):
                trees.append(cur_tree)
                ls = Popen(['git', 'ls-tree', cur_tree], stdout=PIPE, cwd=self.dir)
                grep = Popen(['grep', '040000.*' + dirs[i]], stdin=ls.stdout, stdout=PIPE)
                ls.stdout.close()
                cur_tree = grep.communicate()[0].decode().strip().split('\t')[0].split(' ')[-1]

        trees.append(cur_tree)
        ls = Popen(['git', 'ls-tree', cur_tree], stdout=PIPE, cwd=self.dir)
        result = ls.communicate()[0].decode().strip()

        return trees, result

    @staticmethod
    def _edit_ref(lines: str, old_sha: str, new_sha: str):
        lines = re.sub('160000 commit ' + old_sha, '160000 commit ' + new_sha, lines)
        return lines

    def _mk_tree(self, name: str, trees: list, lines: str):
        edited_lines = Repository._edit_ref(lines, self.children[name]['old_sha'], self.children[name]['new_sha'])

        mk = Popen(['git', 'mktree'], stdin=PIPE, stdout=PIPE, cwd=self.dir)
        cur_new_tree = mk.communicate(input=edited_lines.encode())[0].decode().strip()

        size = len(trees)
        if size > 1:
            for i in range(size - 2, -1, -1):
                ls = Popen(['git', 'ls-tree', trees[i]], stdout=PIPE, cwd=self.dir)
                sed = Popen(['sed', 's/040000 tree ' + trees[i + 1] + '/040000 tree ' + cur_new_tree + '/'],
                            stdin=ls.stdout, stdout=PIPE)
                mk = Popen(['git', 'mktree'], stdin=sed.stdout, stdout=PIPE, cwd=self.dir)
                ls.stdout.close()
                sed.stdout.close()
                cur_new_tree = mk.communicate(input=cur_new_tree.encode())[0].decode().strip()

        return cur_new_tree

    def _hash_commit(self):
        cat = Popen(['git', 'cat-file', 'commit', self.old_sha], stdout=PIPE, cwd=self.dir)
        sed = Popen(['sed', 's/tree ' + self.old_tree + '/tree ' + self.new_tree + '/'], stdin=cat.stdout, stdout=PIPE)
        com = Popen(['git', 'hash-object', '-t', 'commit', '-w', '--stdin'],
                    stdin=sed.stdout, stdout=PIPE, cwd=self.dir)
        cat.stdout.close()
        sed.stdout.close()
        sha = com.communicate()[0].decode().strip()
        return sha
