import os


class Config:
    # Getting application environmental variables
    FLASK_RUN_HOST = os.environ.get('FLASK_RUN_HOST')
    FLASK_RUN_PORT = os.environ.get('FLASK_RUN_PORT')
    APP__DIR = os.path.dirname(os.path.realpath(__file__))
    # Getting gitea server environmental variables
    SERVER__HOST = os.environ.get('SERVER__HOST')
    SERVER__PORT = os.environ.get('SERVER__PORT')
    SERVER__METHOD = os.environ.get('SERVER__METHOD')
    SERVER__BASE_URL = os.environ.get('SERVER__BASE_URL')
    SERVER__TOKEN = os.environ.get('SERVER__TOKEN')
    SERVER__ADMIN_USERNAME = os.environ.get('SERVER__ADMIN_USERNAME')
    SERVER__ADMIN_EMAIL = os.environ.get('SERVER__ADMIN_EMAIL')
