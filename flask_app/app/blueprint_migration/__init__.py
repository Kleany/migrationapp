from flask import Blueprint

bp = Blueprint('migration', __name__, template_folder='templates')

from app.blueprint_migration import routes
