from flask import request, render_template, current_app
import logging

from src.graph import make_graph
from src.repository import Repository
from src.forms import URLForm

from app.blueprint_migration import bp
from app import Gitea

tree = []
level = 0

graph_layers = []
target_name = None


@bp.route('/', methods=['GET', 'POST'])
def scan():
    global level
    global tree
    global graph_layers
    global target_name

    form = URLForm(request.form)
    if request.method == 'POST' and form.validate():
        logging.info(f'==========Starting scanning==========')
        url = form.url.data
        sha = form.sha.data

        logging.info(f'=====Level {level}=====')
        target_repo = Repository(current_app.config, url, sha, level, Gitea)
        try:
            # Processing target repo
            logging.info(f'Processing repo on url [{url}] with required commit [{sha}]')
            target_repo.clone()
            target_repo.get_children_info()
        except ValueError as err:
            logging.error(f'Unable to process repo on url [{url}] with required commit [{sha}]')
            target_repo.rm_repo()
            return render_template('scan_failure.html', url=url, sha=sha, code=err.args[0], response=err.args[1])
        else:
            current_level_list = [target_repo]
            tree.append(current_level_list)
            target_name = target_repo.dir.split('/')[-1]

            # Nested submodule levels processing loop
            while True:
                # Checking if next submodule level exists
                prev_list = tree[level]
                stop = True
                for repo in prev_list:
                    if repo.has_children:
                        stop = False
                        break
                if stop:
                    break

                # Processing next submodule level
                level += 1
                current_level_list = list()
                logging.info(f'=====Level {level}=====')

                # Current repo children loop
                graph_dict = {}
                for repo in prev_list:
                    for name in repo.children:
                        url = repo.children[name]['url']
                        sha = repo.children[name]['old_sha']
                        child = Repository(current_app.config, url, sha, level, Gitea)

                        try:
                            logging.info(f'Processing child repo on url [{url}] with required commit [{sha}]')
                            child.clone()
                            child.get_children_info()
                        except ValueError as err:
                            logging.error(f'Unable to process child repo on url [{url}] with required commit [{sha}]')
                            child.rm_repo()
                            return render_template('scan_failure.html', url=url, sha=sha, code=err.args[0],
                                                   response=err.args[1])
                        else:
                            graph_dict[child.dir.split('/')[-1]] = repo.dir.split('/')[-1]
                            current_level_list.append(child)
                graph_layers.append(graph_dict)
                tree.append(current_level_list)
            logging.info(f'==========Scan of a repo [{target_repo.name}] completed successfully==========')

            try:
                hierarchy = make_graph(graph_layers, target_name)
            except:
                for rm_list in tree:
                    for repo in rm_list:
                        repo.rm_repo()
                return render_template('scan_failure.html', url=url, sha=sha)
            else:
                return render_template('scan_success.html', graph=hierarchy)
    else:
        return render_template('scan.html')


@bp.route('/migrate', methods=['GET'])
def migrate():
    global level
    global tree
    global graph_layers
    global target_name

    if request.method == 'GET':
        logging.info(f'==========Starting migration==========')
        cur_level = level
        while cur_level >= 0:
            repo_level = tree[cur_level]
            logging.info(f'=====Level {cur_level}=====')
            for repo in repo_level:
                logging.info(f'=====Migrating repo [{repo.name}]..')
                if cur_level == level:
                    prev_level = []
                else:
                    prev_level = tree[cur_level + 1]
                try:
                    repo.migrate(prev_level)
                except ValueError as err:
                    logging.error(f'Migration of repo [{repo.name}] failed')
                    for rm_list in tree:
                        for rm_repo in rm_list:
                            rm_repo.rm_repo()
                    return render_template('migration_failure.html', code=err.args[0], response=err.args[1])
                else:
                    logging.info(f'Migration of repo [{repo.name}] successful')
                repo.rm_repo()
            cur_level -= 1
        logging.info(f'==========Migration of a repo [{tree[0][0].name}] completed successfully==========')
        level = 0
        tree = []
        graph_layers = []
        target_name = None
        return render_template('migration_success.html')
