from flask import Blueprint

bp = Blueprint('main', __name__, template_folder='templates')

from app.blueprint_main import routes
