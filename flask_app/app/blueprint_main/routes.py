from flask import render_template

from app.blueprint_main import bp


@bp.route('/')
def start_func():
    return render_template('main_page.html')
