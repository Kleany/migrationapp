from flask import request, render_template

from src.forms import OrgForm, RepoForm, CommitForm
from app.blueprint_api import bp
from app import Gitea


@bp.route('/')
def api():
    return render_template('gitea_api.html')


@bp.route('/clean', methods=['GET'])
def clean():
    Gitea.clean_server()
    return render_template('clean_server/clean_success.html')


@bp.route('/create_org', methods=['GET', 'POST'])
def create_org():
    form = OrgForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        try:
            response = Gitea.create_org(name=username)
        except ValueError as err:
            return render_template('create_org/create_org_fail.html', username=username, code=err.args[0],
                                   response=err.args[1])
        else:
            return render_template('create_org/create_org_success.html', username=username, response=response)
    else:
        return render_template('create_org/create_org.html')


@bp.route('/delete_org', methods=['GET', 'POST'])
def delete_org():
    form = OrgForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        try:
            response = Gitea.delete_org(name=username)
        except ValueError as err:
            return render_template('delete_org/delete_org_fail.html', username=username, code=err.args[0],
                                   response=err.args[1])
        else:
            return render_template('delete_org/delete_org_success.html', username=username, response=response)
    else:
        return render_template('delete_org/delete_org.html')


@bp.route('/get_org', methods=['GET', 'POST'])
def get_org():
    form = OrgForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        try:
            response = Gitea.get_org(name=username)
        except ValueError as err:
            return render_template('get_org/get_org_fail.html', username=username, code=err.args[0],
                                   response=err.args[1])
        else:
            return render_template('get_org/get_org_success.html', username=username, response=response)
    else:
        return render_template('get_org/get_org.html')


@bp.route('/create_repo', methods=['GET', 'POST'])
def create_repo():
    form = RepoForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        reponame = form.reponame.data
        try:
            response = Gitea.create_repo(username=username, reponame=reponame)
        except ValueError as err:
            return render_template('create_repo/create_repo_fail.html', username=username, reponame=reponame,
                                   code=err.args[0], response=err.args[1])
        else:
            return render_template('create_repo/create_repo_success.html', username=username, reponame=reponame,
                                   response=response)
    else:
        return render_template('create_repo/create_repo.html')


@bp.route('/get_repo', methods=['GET', 'POST'])
def get_repo():
    form = RepoForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        reponame = form.reponame.data
        try:
            response = Gitea.get_repo(username=username, reponame=reponame)
        except ValueError as err:
            return render_template('get_repo/get_repo_fail.html', username=username, reponame=reponame,
                                   code=err.args[0], response=err.args[1])
        else:
            return render_template('get_repo/get_repo_success.html', username=username, reponame=reponame,
                                   response=response)
    else:
        return render_template('get_repo/get_repo.html')


@bp.route('/delete_repo', methods=['GET', 'POST'])
def delete_repo():
    form = RepoForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        reponame = form.reponame.data
        try:
            response = Gitea.delete_repo(username=username, reponame=reponame)
        except ValueError as err:
            return render_template('delete_repo/delete_repo_fail.html', username=username, reponame=reponame,
                                   code=err.args[0], response=err.args[1])
        else:
            return render_template('delete_repo/delete_repo_success.html', username=username, reponame=reponame,
                                   response=response)
    else:
        return render_template('delete_repo/delete_repo.html')


@bp.route('/get_commit', methods=['GET', 'POST'])
def get_commit():
    form = CommitForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        reponame = form.reponame.data
        commit = form.commit.data
        try:
            response = Gitea.get_commit(username=username, reponame=reponame, commit=commit)
        except ValueError as err:
            return render_template('get_commit/get_commit_fail.html', username=username, reponame=reponame,
                                   commit=commit, code=err.args[0], response=err.args[1])
        else:
            return render_template('get_commit/get_commit_success.html', username=username, reponame=reponame,
                                   commit=commit, response=response)
    else:
        return render_template('get_commit/get_commit.html')
