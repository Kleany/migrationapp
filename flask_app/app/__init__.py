from flask import Flask
from os import environ
import logging

from src.gitea_manager import GiteaManager
from config import Config

logging.getLogger('werkzeug').setLevel(logging.CRITICAL)
logging.basicConfig(level=logging.INFO, filename=Config.APP__DIR+'/app.log', filemode='w',
                    format='%(asctime)s.%(msecs)03d | %(levelname) 7s | %(funcName) 17s() | %(message)s',
                    datefmt='%H:%M:%S')

Gitea = None


#  Flask application factory function
def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    app.secret_key = environ.get('FLASK_SECRET_KEY')

    global Gitea
    Gitea = GiteaManager(app.config)

    from app.blueprint_main import bp as main_bp
    app.register_blueprint(main_bp)

    from app.blueprint_api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from app.blueprint_migration import bp as migration_bp
    app.register_blueprint(migration_bp, url_prefix='/migrate')

    print('SERVER URL:\thttp://' + app.config['SERVER__HOST'] + ':' + app.config['SERVER__PORT'])
    print('APP URL:\thttp://' + app.config['FLASK_RUN_HOST'] + ':' + app.config['FLASK_RUN_PORT'])
    return app
